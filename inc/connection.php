<?php

/**
 * @package: MySql Connection Class (PDO)
 */

class DBConnection
{
	// Settings (DSN)
	protected $driver, $host, $name, $charset;
	// Credentials
	protected $user, $pass;
	// Data Source Name
    protected $dsn;
    // Database Handle
	protected $dbh;

	public function __construct() {
		require_once( 'database.config.php' );

		// Settings (DSN)
		$this->driver   = DB_DRIVER;
		$this->host     = DB_HOST;
		$this->name     = DB_NAME;
		$this->charset  = DB_CHARSET;
		// Credentials
		$this->user     = DB_USER;
		$this->pass     = DB_PASS;
	}

	public function getConnection() {
		// Check if connection is already established
		if ( $this->dbh == NULL ) {
			$this->dsn = "$this->driver:host=$this->host;dbname=$this->name;charset=$this->charset";
			try {
				$this->dbh = new PDO($this->dsn,$this->user, $this->pass);
				$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
			} catch ( PDOException $error ) {
				echo 'Connection failed: ' . $error->getMessage();
				exit;
			}
		}
		return $this->dbh;
	}

}